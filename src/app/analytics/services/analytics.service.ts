/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from 'src/app/services/env.service';

@Injectable({
  providedIn: 'root',
})
export class AnalyticsService {
  private baseUrl: string = localStorage.getItem('baseUrl');
  private token: string = localStorage.getItem('token');
  private header: any;
  constructor(private http: HttpClient, private env: EnvService) {
    this.header = new HttpHeaders({
      Authorization: 'JWT ' + this.token,
    });
  }

  getStudentAnalytics(date: string) {
    // const token = localStorage.getItem("token");
    const subject = localStorage.getItem('teacher');
    const class_uuid = localStorage.getItem('classId');
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + token,
    // });
    return this.http
      .get(
        this.baseUrl +
          `/api/v1/video/watched/list/?subject=${subject}&class_uuid=${class_uuid}&date=${date}`,
        { headers: this.header }
      )
      .pipe(tap((subjects) => subjects));
  }
}
