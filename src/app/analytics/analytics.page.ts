import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './services/analytics.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.page.html',
  styleUrls: ['./analytics.page.scss'],
})
export class AnalyticsPage implements OnInit {
  public myDate: string = new Date().toISOString();
  data: any;
  constructor(private analyticsService: AnalyticsService) {}

  ngOnInit() {
    console.log(this.myDate);
    this.analyticsService.getStudentAnalytics(this.myDate).subscribe((data) => {
      this.data = data;
      console.log(data);
    });
  }

  showdate() {
    console.log(this.myDate);
    this.analyticsService.getStudentAnalytics(this.myDate).subscribe((data) => {
      this.data = data;
    });
  }
}
