import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from '../services/env.service';

@Injectable({
  providedIn: 'root',
})
export class AccountsService {
  private baseUral: string = localStorage.getItem('baseUrl');
  private token: string = localStorage.getItem('token');
  constructor(private http: HttpClient, private env: EnvService) {}

  getProfileDetails() {
    const headers = new HttpHeaders({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      Authorization: 'JWT ' + this.token,
    });
    return this.http
      .get(this.baseUral + '/api/v1/accounts/profile/details/', {
        // eslint-disable-next-line object-shorthand
        headers: headers,
      })
      .pipe(tap((profile) => profile));
  }
}
