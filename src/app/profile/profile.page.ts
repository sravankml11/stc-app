import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AccountsService } from './accounts.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  profile: any;
  constructor(
    private accounts: AccountsService,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.loadingController
      .create({
        backdropDismiss: true,
      })
      .then((loadingEl) => {
        loadingEl.present();
      });
    this.accounts.getProfileDetails().subscribe((data) => {
      this.profile = data;
      this.loadingController.dismiss();
    });
  }
}
