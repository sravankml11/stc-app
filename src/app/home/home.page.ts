/* eslint-disable no-eval */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../profile/accounts.service';
import { GlobalService } from '../services/global.service';
import { ChaptersService } from './service/chapters.service';
import { SubjectService } from './service/subject.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  chapters: any;
  subjects: any;
  subjectChoice: string;
  backButtonSubscription: any;
  segmentValue = 'all';
  thumbnailImage: any;
  checkLaterItem: any;
  subjectName: string;
  notClearItems: any;
  gotItItems: any;
  isTeacher: string;
  subject_val: string;

  constructor(
    private chapter: ChaptersService,
    private subjectService: SubjectService,
    private accounts: AccountsService,
    private globalService: GlobalService
  ) {
    this.isTeacher = localStorage.getItem('teacher');
    this.getUserData();
  }

  ngOnInit() {
    var subject_choice = localStorage.getItem('subject');
    this.subject_val = subject_choice;
    this.subjectName = localStorage.getItem('subjectName');
    this.accounts.getProfileDetails().subscribe((data) => {
      localStorage.setItem('classId', data[0].class_name);
      if (data[0].teacher) {
        localStorage.setItem('teacher', data[0].teacher);
      }

      // this.profile = data;
    });
    // this.backButtonSubscription = this.platform.backButton.subscribe(() => {
    //   navigator["app"].exitApp();
    // });

    var subject_choice = localStorage.getItem('subject');
    this.subjectChoice = subject_choice;
    this.thumbnailImage = this.globalService.getSubjectImage(subject_choice);
  }

  async getUserData() {
    this.getSubjectList();
    this.getChapterList();
  }
  getSubjectList() {
    this.subjectService.getSubject().subscribe((data) => {
      this.subjects = data;
      console.log(data);
    });
  }
  getChapterList() {
    var subject_choice = localStorage.getItem('subject');

    this.chapter.getChapters(subject_choice).subscribe((data) => {
      this.chapters = data;
    });
  }
  segmentChanged(ev: any) {
    this.segmentValue = ev.detail.value;
    var subject_choice = localStorage.getItem('subject');
    this.subject_val = subject_choice;
    if (this.segmentValue === 'later') {
      this.chapter.getCheckLater(subject_choice).subscribe((data) => {
        this.checkLaterItem = data;
      });
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      setTimeout(() => {
        eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      }, 1000);
    } else if (this.segmentValue === 'gotIt') {
      this.subjectService
        .getVideoActionStats({ gotIt: true })
        .subscribe((data) => {
          this.gotItItems = data;
        });
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      setTimeout(() => {
        eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      }, 1000);
    }
    if (this.segmentValue === 'notClear') {
      this.subjectService
        .getVideoActionStats({ notClear: true })
        .subscribe((data) => {
          this.notClearItems = data;
        });
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      setTimeout(() => {
        eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      }, 1000);
    }
    console.log(this.segmentValue);
  }

  subjectItem(subjectChoice: any) {
    localStorage.setItem('subject', subjectChoice.detail.value.subject_choice);
    localStorage.setItem('subjectName', subjectChoice.detail.value.name);

    this.subjectName = subjectChoice.detail.value.name;
    this.thumbnailImage = this.globalService.getSubjectImage(
      subjectChoice.detail.value.subject_choice
    );
    this.chapter
      .getChapters(subjectChoice.detail.value.subject_choice)
      .subscribe((data) => {
        this.chapters = data;
      });
  }
}
