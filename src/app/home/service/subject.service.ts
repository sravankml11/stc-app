/* eslint-disable no-var */
/* eslint-disable object-shorthand */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/quotes */
// eslint-disable-next-line no-var
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from 'src/app/services/env.service';

@Injectable({
  providedIn: 'root',
})
export class SubjectService {
  private baseUrl: string = localStorage.getItem('baseUrl');
  private token: string = localStorage.getItem('token');
  private header: any;
  constructor(private http: HttpClient, private env: EnvService) {
    this.header = new HttpHeaders({
      Authorization: 'JWT ' + this.token,
    });
  }

  getSubject() {
    const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + token,
    // });
    console.log(this.baseUrl);
    return this.http
      .get(this.baseUrl + '/api/v1/subject/list/', { headers: this.header })
      .pipe(tap((subject) => subject));
  }

  getVideoActionStats(data: any) {
    // const token = localStorage.getItem("token");
    var teacher = localStorage.getItem('teacher');
    const subject = localStorage.getItem('subject');
    const class_uuid = localStorage.getItem('classId');
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + token,
    // });
    if (!teacher) {
      teacher = '';
    }
    const params = new HttpParams().set('teacher', teacher);
    // eslint-disable-next-line no-var
    var endPoint = `/api/v1/video/action/stats/?subject=${subject}&class_uuid=${class_uuid}&not_clear=${data.notClear}`;
    if (data.gotIt) {
      // eslint-disable-next-line no-var
      var endPoint = `/api/v1/video/action/stats/?subject=${subject}&class_uuid=${class_uuid}&got_it=${data.gotIt}`;
    }

    return this.http
      .get(this.baseUrl + endPoint, {
        headers: this.header,
        params: params,
      })
      .pipe(tap((subjects) => subjects));
  }
}
