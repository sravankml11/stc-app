/* eslint-disable object-shorthand */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ChaptersService {
  isLoggedIn = false;
  private baseUrl: string = localStorage.getItem('baseUrl');
  private token: string = localStorage.getItem('token');
  private header: any;

  constructor(private http: HttpClient) {
    this.header = new HttpHeaders({
      Authorization: 'JWT ' + this.token,
    });
  }

  getChapters(subjectChoice) {
    return this.http
      .get(this.baseUrl + `/api/v1/chapter/list/?subject=${subjectChoice}`, {
        headers: this.header,
      })
      .pipe(tap((chapters) => chapters));
  }

  getTopics(chapterId: string) {
    return this.http
      .get(
        this.baseUrl + `/api/v2/chapter/topic/list/?chapter_id=${chapterId}`,
        {
          headers: this.header,
        }
      )
      .pipe(tap((topics) => topics));
  }

  VideoAccessEnable(topicId: string) {
    return this.http
      .get(this.baseUrl + `/api/v1/video/${topicId}/accessible/`, {
        headers: this.header,
      })
      .pipe(tap((topics) => topics));
  }

  getAnswers(topicId: string) {
    // const token = localStorage.getItem("token");
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + this.token,
    // });
    return this.http
      .get(this.baseUrl + `/api/v1/chapter/topic/answer/?topic_id=${topicId}`, {
        headers: this.header,
      })
      .pipe(tap((topics) => topics));
  }

  getExercise(chapterId: string) {
    // const token = localStorage.getItem("token");
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + this.token,
    // });
    return this.http
      .get(
        this.baseUrl +
          `/api/v1/chapter/exercises/list/?chapter_id=${chapterId}`,
        {
          headers: this.header,
        }
      )
      .pipe(tap((exercises) => exercises));
  }

  postVideoAction(
    gotIt: boolean,
    notClear: boolean,
    checkLater: boolean,
    question: any
  ) {
    // const token = localStorage.getItem("token");
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + this.token,
    // });
    var body = {
      question: question,
      check_later: checkLater,
      got_it: gotIt,
      not_clear: notClear,
    };
    this.http
      .post(this.baseUrl + '/api/v1/create/video/action/', body, {
        headers: this.header,
      })
      .subscribe(
        (val) => {
          console.log('POST call successful value returned in body', val);
        },
        (response) => {
          console.log('POST call in error', response);
        }
      );
  }

  getCheckLater(subject_choice: string) {
    // const token = localStorage.getItem("token");
    // const headers = new HttpHeaders({
    //   Authorization: "JWT " + this.token,
    // });
    return this.http
      .get(
        this.baseUrl + `/api/v1/check/later/list/?subject=${subject_choice}`,
        {
          headers: this.header,
        }
      )
      .pipe(tap((topic) => topic));
  }
}
