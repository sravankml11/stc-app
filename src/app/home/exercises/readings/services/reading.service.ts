/* eslint-disable object-shorthand */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from 'src/app/services/env.service';
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject,
} from '@awesome-cordova-plugins/file-transfer/ngx';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ReadingService {
  private baseUrl: string = localStorage.getItem('baseUrl');
  private token: string = localStorage.getItem('token');
  private header: any;
  constructor(
    private http: HttpClient,
    private env: EnvService, // private file: File
    private transfer: FileTransfer
  ) {
    this.header = new HttpHeaders({
      Authorization: 'JWT ' + this.token,
    });
  }

  getUserReadingContent(chapterId: string) {
    return this.http
      .get(
        this.baseUrl +
          `/api/v1/get/student/reading/content/?chapterId=${chapterId}`,
        { headers: this.header }
      )
      .pipe(tap((subject) => subject));
  }

  TeacherStudentReadingList(chapterId: string, studentId: string) {
    return this.http
      .get(
        this.baseUrl +
          `/api/v1/get/student/reading/content/?chapterId=${chapterId}&studentId=${studentId}`,
        { headers: this.header }
      )
      .pipe(tap((readingList) => readingList));
  }

  getStudentReadingList(chapterId: string) {
    return this.http
      .get(
        this.baseUrl +
          `/api/v1/student/reading/list/get/?chapterId=${chapterId}`,
        { headers: this.header }
      )
      .pipe(tap((studentList) => studentList));
  }

  SubmitUserReadingContentForm(
    chapterId: string,
    recordedFile: any,
    duration: any,
    minType: string
  ) {
    const newHeader = new HttpHeaders({
      Authorization: 'JWT ' + this.token,
      // "Content-Type": "multipart/form-data",
    });
    var param = new HttpParams().set('chapterId', chapterId);
    const formData = new FormData();
    // formData.append("file", recordedFile, "sample.wav");
    // var data = { file: recordedFile };
    const fileTransfer = this.transfer.create();
    var date = new Date();
    const file_name = date.toUTCString();
    const extension = minType.split('/')[1];
    const options: FileUploadOptions = {
      fileKey: 'file',
      fileName: `${file_name}.${extension}`,
      headers: { Authorization: 'JWT ' + this.token },
      mimeType: minType,
      chunkedMode: false,
      params: { chapterId: chapterId, duration: duration },
    };
    console.log(recordedFile, options);

    return fileTransfer.upload(
      recordedFile,
      encodeURI(this.baseUrl + '/api/v1/submit/student/lesson/reading/'),
      options
    );
  }

  deleteRecording(uuid: string) {
    return this.http
      .delete(this.baseUrl + `/api/v1/student/recording/delete/${uuid}/`, {
        headers: this.header,
      })
      .pipe(tap((subject) => subject));
  }
}
