/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/quotes */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReadingService } from './services/reading.service';
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject,
} from '@awesome-cordova-plugins/file-transfer/ngx';
import { AlertController, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import {
  VoiceRecorder,
  VoiceRecorderPlugin,
  RecordingData,
  GenericResponse,
  CurrentRecordingStatus,
} from 'capacitor-voice-recorder';
import { DictionaryModelPage } from './dictionary-model/dictionary-model.page';

@Component({
  selector: 'app-readings',
  templateUrl: './readings.page.html',
  styleUrls: ['./readings.page.scss'],
})
export class ReadingsPage implements OnInit {
  contents: any = [];
  chapterId: string;
  contentData: any;
  recordingStatus = false;
  counter = 0;
  timerInterval: any;
  recordDuration: string;
  data: any;
  search: string;
  recordingPaused = false;
  modelData: any;
  constructor(
    private readingService: ReadingService,
    private route: ActivatedRoute,
    private transfer: FileTransfer,
    public alertController: AlertController,
    private globalService: GlobalService,
    public modalController: ModalController
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      this.chapterId = paramMap.get('chapterId');
    });
    // this.globalService.minutes.subscribe((message) => console.log(message));
    this.getUserReadingContent(this.chapterId);
    VoiceRecorder.hasAudioRecordingPermission().then(
      (result: GenericResponse) => {
        console.log(result.value);
        if (!result.value) {
          VoiceRecorder.requestAudioRecordingPermission();
        }
      }
    );
  }

  getUserReadingContent(chapterId: string) {
    this.readingService.getUserReadingContent(chapterId).subscribe((data) => {
      this.contents = data;
    });

    // this.globalService.minutes.subscribe((message) => console.log(message));
    // this.contents = [
    //   {
    //     url: "https://stc-lesson-recordings.s3.amazonaws.com/ambika/2021/X/priya10/History/Nationalism%20in%20India/Mon%2C%2015%20Nov%202021%2007%3A11%3A27%20GMT.aac?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAXOWBXSBLBCNBX3RC%2F20211115%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20211115T071430Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=521e6dc1147c303e40aa87458da513d36c96dbb2373cbed3ceae86e19e6f64da",
    //     uuid: "74c7435b-366a-4c7b-8e32-c3c0e4c32e39",
    //   },
    //   {
    //     url: "https://stc-lesson-recordings.s3.amazonaws.com/ambika/2021/X/priya10/History/Nationalism%20in%20India/Mon%2C%2015%20Nov%202021%2006%3A54%3A41%20GMT.aac?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAXOWBXSBLBCNBX3RC%2F20211115%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20211115T071430Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=9f3b4f272447dfafe5e1e50a328aa03cae3b39e6cb9781b621560d5ad9c553ba",
    //     uuid: "223e2490-6741-4243-9496-688d5511f5d2",
    //   },
    //   {
    //     url: "https://stc-lesson-recordings.s3.amazonaws.com/ambika/2021/X/priya10/History/Nationalism%20in%20India/Mon%2C%2015%20Nov%202021%2006%3A48%3A21%20GMT.aac?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAXOWBXSBLBCNBX3RC%2F20211115%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20211115T071430Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=93ba885e253214bb573d3d3ef1d83bc8941099b38db835e68d61f55f08dffbca",
    //     uuid: "a307fe13-dbb0-4b6a-bca2-1967ab83520f",
    //   },
    // ];
  }

  StopRecording() {
    VoiceRecorder.stopRecording().then(async (result: RecordingData) => {
      // const recordings = Filesystem.writeFile({
      //   path: "sample.wav",
      //   data: result.value.recordDataBase64,
      //   directory: Directory.External,
      // });
      // console.log("recording file details", (await recordings).uri);

      const contentType =
        `data:audio/${result.value.mimeType};base64,` +
        result.value.recordDataBase64;
      const fileTransfer: FileTransferObject = this.transfer.create();

      this.readingService
        .SubmitUserReadingContentForm(
          this.chapterId,
          contentType,
          result.value.msDuration,
          result.value.mimeType
        )
        .then(
          (data) => {
            // success

            this.loadContent(data);
          },
          (err) => {
            console.log(err);
          }
        );
      // .subscribe((data) => console.log(data));

      this.recordingStatus = false;
      this.startCountdown();
    });
  }

  async loadContent(data: any) {
    this.contents.push(JSON.parse(data.response));
    console.log(this.contents);
  }

  recordAudio() {
    // let options: CaptureAudioOptions = { limit: 1, duration: 10 };
    // console.log(this.mediaCapture.supportedAudioModes);
    // const audioFile = this.mediaCapture
    //   .captureAudio(options)
    //   .then((data: MediaFile[]) => {
    //     console.log(data);
    //   });
    // console.log(audioFile);
    VoiceRecorder.hasAudioRecordingPermission().then(
      (result: GenericResponse) => {
        console.log(result.value);
        if (!result.value) {
          VoiceRecorder.requestAudioRecordingPermission();
          return false;
        }
      }
    );

    VoiceRecorder.startRecording().then((data) => console.log(data));
    this.recordingStatus = true;

    const interval = setInterval(() => {
      this.startCountdown();
    }, 1000);
  }

  pauseRecording() {
    this.recordingPaused = true;
    VoiceRecorder.pauseRecording()
      .then((result: GenericResponse) => console.log(result.value))
      .catch((error) => console.log(error));
    this.startCountdown();
  }

  resumeRecording() {
    this.recordingPaused = false;
    VoiceRecorder.resumeRecording()
      .then((result: GenericResponse) => console.log(result.value))
      .catch((error) => console.log(error));
  }

  startCountdown() {
    if (this.recordingStatus && this.recordingPaused) {
      // debugger;
      this.counter = this.counter;
      this.timerInterval = this.timerInterval;
      return;
    } else if (!this.recordingStatus) {
      // debugger;
      this.counter = 0;
      this.timerInterval = '';
      return;
    }
    this.counter++;
    const minutes = Math.floor(this.counter / 60);
    const second = (this.counter % 60).toString().padStart(2, '0');
    this.timerInterval = `${minutes}:${second}`;
  }

  async presentAlert(recordingUuid: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Delete',
      subHeader: 'Are you sure you want to delete this recording',
      // message: "This is an alert message.",
      buttons: ['Cancel', 'Yes'],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
    if (role !== 'cancel' && role !== 'backdrop') {
      this.readingService.deleteRecording(recordingUuid).subscribe((data) => {
        this.contents = this.contents.filter((dataValue) => {
          if (dataValue.uuid !== recordingUuid) {
            return dataValue;
          }
        });
        console.log(data);
      });
    } else {
      return true;
    }
  }

  async findMeaning() {
    const modal = await this.modalController.create({
      component: DictionaryModelPage,
      cssClass: 'my-custom-class',
      initialBreakpoint: 0.5,
      breakpoints: [0, 0.5, 1],
      componentProps: {
        search: this.search,
      },
    });
    return await modal.present();
  }
}
