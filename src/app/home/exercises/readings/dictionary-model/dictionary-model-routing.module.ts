import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DictionaryModelPage } from './dictionary-model.page';

const routes: Routes = [
  {
    path: '',
    component: DictionaryModelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DictionaryModelPageRoutingModule {}
