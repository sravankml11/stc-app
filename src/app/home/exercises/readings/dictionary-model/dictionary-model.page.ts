import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-dictionary-model',
  templateUrl: './dictionary-model.page.html',
  styleUrls: ['./dictionary-model.page.scss'],
})
export class DictionaryModelPage implements OnInit {
  @Input() search: string;
  meaning: any;
  error: boolean;
  constructor(
    private modalController: ModalController,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.http
      .get(`https://api.dictionaryapi.dev/api/v2/entries/en/${this.search}`, {})
      .pipe(tap((data) => data))
      .subscribe(
        (value) => {
          console.log(value);
          this.meaning = value;
        },
        (error) => {
          console.log(error);
          this.error = true;
        }
      );
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true,
    });
  }
}
