import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DictionaryModelPageRoutingModule } from './dictionary-model-routing.module';

import { DictionaryModelPage } from './dictionary-model.page';
import { AudioSafePipe } from 'src/app/services/pips/audio-safe.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DictionaryModelPageRoutingModule,
  ],
  declarations: [DictionaryModelPage, AudioSafePipe],
})
export class DictionaryModelPageModule {}
