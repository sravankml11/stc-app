import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReadingListPage } from './reading-list.page';

const routes: Routes = [
  {
    path: '',
    component: ReadingListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReadingListPageRoutingModule {}
