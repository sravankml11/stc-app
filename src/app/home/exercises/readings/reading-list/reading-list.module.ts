import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReadingListPageRoutingModule } from './reading-list-routing.module';

import { ReadingListPage } from './reading-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReadingListPageRoutingModule
  ],
  declarations: [ReadingListPage]
})
export class ReadingListPageModule {}
