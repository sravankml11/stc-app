/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { ChaptersService } from '../service/chapters.service';
import { ReadingService } from './readings/services/reading.service';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.page.html',
  styleUrls: ['./exercises.page.scss'],
})
export class ExercisesPage implements OnInit {
  chapterId: string;
  topics: any;
  duration: any;
  exercises: any;
  chapterName: string;
  teacher: string;
  subject: string;
  topicLength: any;
  readingMinutes: number;
  content: any;
  chapter_name: string;
  last_read: string;
  readingList: any;

  constructor(
    private route: ActivatedRoute,
    private chapter: ChaptersService,
    public loadingController: LoadingController,
    private router: Router,
    private platform: Platform,
    private globalService: GlobalService,
    private readingServices: ReadingService
  ) {
    this.loadingController
      .create({
        // message: "Creating place...",
        backdropDismiss: true,
      })
      .then((loadingEl) => {
        loadingEl.present();
      });

    this.route.paramMap.subscribe((paramMap) => {
      this.chapterId = paramMap.get('chapterId');
      if (!this.topics) {
        this.getTopic(this.chapterId);
        // this.getExercise(this.chapterId);
      }
    });
  }

  ngOnInit() {
    this.teacher = localStorage.getItem('teacher');
    this.subject = localStorage.getItem('subject');
    this.getReadingList(this.chapterId);
  }

  getTopic(chapterId) {
    this.chapter.getTopics(chapterId).subscribe((data) => {
      this.topics = data['topics'];
      this.content = data;
      this.last_read = data['last_read'];
      this.chapter_name = data['chapter_name'];
      this.topicLength = data['topics'].length;
      this.duration = data['duration'];
      // this.globalService.publishNewMinutes(data["reading_percentage"]);

      // this.globalService.getNewMinutes().subscribe((message) => {
      //   this.duration = message.readingMinutes;
      // });
      this.loadingController.dismiss();
    });
  }

  getReadingList(chapterId) {
    this.readingServices.getStudentReadingList(chapterId).subscribe((data) => {
      this.readingList = data;
      console.log(data);
    });
  }

  getExercise(chapterId) {
    this.chapter.getExercise(chapterId).subscribe((data) => {
      this.exercises = data;
      this.loadingController.dismiss();
    });
  }

  enableTopic(topicId: string) {
    this.chapter.VideoAccessEnable(topicId).subscribe((data) => {
      console.log(data);
    });
  }

  ionViewDidEnter() {
    // eslint-disable-next-line no-eval
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    setTimeout(() => {
      // eslint-disable-next-line no-eval
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    }, 1000);
  }

  doRefresh(event) {
    this.getTopic(this.chapterId);
    this.getExercise(this.chapterId);
    event.target.complete();
  }

  StarReading() {
    this.router.navigate([`/home/${this.chapterId}/exercises/readings/`]);
  }
}
