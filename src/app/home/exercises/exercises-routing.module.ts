import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExercisesPage } from './exercises.page';

const routes: Routes = [
  {
    path: '',
    component: ExercisesPage,
  },
  {
    path: ':topicId/answers',
    loadChildren: () =>
      import('./answers/answers.module').then((m) => m.AnswersPageModule),
  },
  {
    path: 'reading-list',
    loadChildren: () =>
      import('./readings/reading-list/reading-list.module').then(
        (m) => m.ReadingListPageModule
      ),
  },
  {
    path: 'dictionary-model',
    loadChildren: () =>
      import('./readings/dictionary-model/dictionary-model.module').then(
        (m) => m.DictionaryModelPageModule
      ),
  },
  {
    path: 'readings',
    loadChildren: () => import('./readings/readings.module').then( m => m.ReadingsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExercisesPageRoutingModule {}
