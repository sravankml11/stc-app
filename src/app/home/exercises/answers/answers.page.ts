/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/dot-notation */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ChaptersService } from '../../service/chapters.service';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {
  @ViewChild('iframe') iframe: ElementRef;
  topicId: string;
  answer: any;
  activity: any = { gotIt: false, notClear: false, checkLater: false };
  activityUpdated = false;
  videoUrl!: SafeResourceUrl;
  constructor(
    private route: ActivatedRoute,
    // private navCtrl: NavController,
    private chapter: ChaptersService,
    public loadingController: LoadingController,
    public sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.loadingController
      .create({
        // message: "Creating place...",
        backdropDismiss: true,
      })
      .then((loadingEl) => {
        loadingEl.present();
      });

    this.route.paramMap.subscribe((paramMap) => {
      this.topicId = paramMap.get('topicId');
    });

    this.chapter.getAnswers(this.topicId).subscribe((data) => {
      this.answer = data;
      this.loadingController.dismiss();
      this.activity['checkLater'] = this.answer[0].video_action?.check_later;
      this.activity['gotIt'] = this.answer[0].video_action?.got_it;
      this.activity['notClear'] = this.answer[0].video_action?.not_clear;
      var url = this.answer[0].video;

      this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    });
  }
  ionViewDidEnter() {
    // eslint-disable-next-line no-eval
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    setTimeout(() => {
      // eslint-disable-next-line no-eval
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    }, 1000);

    // var player = new Vimeo.Player(this.iframe.nativeElement);

    // player.on("play", function () {
    //   console.log("played the video!");
    // });
    // console.log("will enter");
  }

  doRefresh(event) {
    console.log('Begin async operation');
    // eslint-disable-next-line no-eval
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    event.target.complete();
  }

  videoActivity(
    gotIt: boolean,
    notClear: boolean,
    checkLater: boolean,
    question: string
  ) {
    if (this.activity.gotIt === true && notClear === true) {
      this.activity['gotIt'] = false;
      this.activity['notClear'] = true;
      gotIt = false;
      notClear = true;
    }

    if (this.activity.notClear === true && gotIt === true) {
      this.activity['notClear'] = false;
      this.activity['gotIt'] = true;
      gotIt = true;
      notClear = false;
    }

    this.activity['checkLater'] = checkLater;
    this.activity['gotIt'] = gotIt;
    this.activity['notClear'] = notClear;

    this.chapter.postVideoAction(gotIt, notClear, checkLater, question);
  }

  bypassSecurityTrustUrl(oldURL: string): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(oldURL);
  }
}
