import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public teacher: string;
  constructor(private navCtrl: NavController) {
    this.teacher = localStorage.getItem('teacher');
  }
  logOut() {
    localStorage.removeItem('token');
    this.navCtrl.navigateBack('/login');
  }
}
