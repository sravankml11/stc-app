/* eslint-disable no-fallthrough */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  private thumbnailImage: string;
  constructor() {}

  getSubjectImage(subject_choice: string) {
    subject_choice = subject_choice.toString();
    switch (subject_choice) {
      case '10':
        this.thumbnailImage = 'assets/images/subject/maths.jpg';
        break;
      case '20':
        this.thumbnailImage = 'assets/images/subject/Physics.jpeg';
      case '30':
        this.thumbnailImage = 'assets/images/subject/chemistry.png';
        break;
      case '40':
        this.thumbnailImage = 'assets/images/subject/biology.jpeg';
        break;
      case '50':
        this.thumbnailImage = 'assets/images/subject/history.jpeg';
        break;
      case '60':
        this.thumbnailImage = 'assets/images/subject/geography.png';
        break;
      case '70':
        this.thumbnailImage = 'assets/images/subject/civics.png';
        break;
      default:
        this.thumbnailImage = 'assets/images/subject/others.png';
        break;
    }
    return this.thumbnailImage;
  }
}
