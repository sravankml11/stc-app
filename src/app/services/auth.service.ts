import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
// import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
// import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;
  token: any;
  constructor(
    private http: HttpClient,
    // private storage: NativeStorage,
    private env: EnvService
  ) {}

  login(username: string, password: string) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const login_data = this.http
      .post(this.env.API_URL + 'api/token/', {
        // eslint-disable-next-line object-shorthand
        username: username,
        // eslint-disable-next-line object-shorthand
        password: password,
      })
      .pipe(
        tap((token) => {
          // eslint-disable-next-line @typescript-eslint/dot-notation
          localStorage.setItem('token', token['access']);
          localStorage.setItem('subject', '10');
          // eslint-disable-next-line @typescript-eslint/dot-notation
          localStorage.setItem('baseUrl', `https://${token['domain']}`);
          // localStorage.setItem('baseUrl', `http://${token['domain']}:8081`);
          localStorage.setItem('subjectName', 'Mathematics');
          this.token = token;
          this.isLoggedIn = true;

          // console.log('data', data);

          return token;
        })
      );
    // let data = this.getUserDetails();
    // console.log(data);
    return login_data;
  }

  getUserDetails() {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      Authorization: 'JWT ' + token,
    });
    const data = this.http
      .get(this.env.API_URL + 'api/v1/accounts/user/details/', {
        // eslint-disable-next-line object-shorthand
        headers: headers,
      })
      .toPromise();
    // .then((data) => {
    //   return data;
    // });
    // .then((data) => {
    //   localStorage.setItem(
    //     'baseUrl',
    //     `https://${data['school'].sub_domain.domain}`
    //   );
    //   console.log(data);
    // });
    return data;
  }
  logout() {
    const headers = new HttpHeaders({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      Authorization: this.token.token_type + ' ' + this.token.access_token,
    });
    return (
      this.http
        // eslint-disable-next-line object-shorthand
        .get(this.env.API_URL + 'auth/logout', { headers: headers })
        .pipe(
          tap((data) => {
            // this.storage.remove('token');
            // this.storage.remove('teacher');
            this.isLoggedIn = false;
            delete this.token;
            return data;
          })
        )
    );
  }
  user() {
    const headers = new HttpHeaders({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      Authorization: this.token.token_type + ' ' + this.token.access_token,
    });
    return (
      this.http
        // eslint-disable-next-line object-shorthand
        .get(this.env.API_URL + 'auth/user', { headers: headers })
        .pipe(tap((user) => user))
    );
  }
  // getToken() {
  //   return this.storage.getItem('token').then(
  //     (data) => {
  //       this.token = data;
  //       if (this.token != null) {
  //         this.isLoggedIn = true;
  //       } else {
  //         this.isLoggedIn = false;
  //       }
  //     },
  //     (error) => {
  //       this.token = null;
  //       this.isLoggedIn = false;
  //     }
  //   );
  // }
}
