import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {
  LoadingController,
  ModalController,
  NavController,
  Platform,
} from '@ionic/angular';
import { AlertService } from '../services/alert.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private loadingCtr: LoadingController,
    private platform: Platform // private globalService: GlobalServiceService
  ) {}

  ngOnInit() {}

  dismissLogin() {
    this.modalController.dismiss();
  }

  async login(form: NgForm) {
    this.loadingCtr
      .create({
        message: 'please wait...',
      })
      .then((loadingEl) => {
        loadingEl.present();
      });
    console.log(form.value.username);
    this.authService
      .login(form.value.username.toLowerCase(), form.value.password)
      .subscribe(
        (data) => {
          // localStorage.setItem("token", data["access"]);
          // var user = await this.authService.getUserDetails();
          // localStorage.setItem(
          //   "baseUrl",
          //   `https://${user["school"].sub_domain.domain}`
          // );
          // console.log("login user data", user);
          // this.alertService.presentToast("Logged In Successfully");
        },
        (error) => {
          console.log(error);
          this.loadingCtr.dismiss();
          this.alertService.presentToast('Invalid Username or Password');
        },
        () => {
          this.dismissLogin();
          this.loadingCtr.dismiss();
          // var teacher = localStorage.getItem("teacher");
          // this.events.publish("teacher", teacher);
          this.navCtrl.navigateRoot('/home');
        }
      );

    this.navCtrl.navigateRoot('/home');
  }
}
